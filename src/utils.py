# small batching tool
from itertools import zip_longest


def batched(f):
    batched = zip_longest(*([f] * 100))
    for batch in batched:
        yield list(filter(None, batch))
